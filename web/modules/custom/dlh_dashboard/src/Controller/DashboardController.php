<?php

namespace Drupal\dlh_dashboard\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for dashboard page.
 */
class DashboardController extends ControllerBase {

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function title() {
    return $this->t('Dashboard');
  }

  /**
   * @return array
   */
  public function build() {
    return [
      '#theme' => 'dlh_dashboard',
      '#content' => [
        $this->t('Dashboard here'),
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
