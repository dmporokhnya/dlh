<?php

namespace Drupal\dlh_user;

use Drupal\user\Entity\User;

/**
 * Class Drupal\dlh_user
 *
 * @package Drupal\dlh_user
 */
class DlhUser extends User {

  /**
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPicture() {
    $picture = '/' . drupal_get_path('theme', 'rad_dlh') . '/assets/img/!logged-user.jpg';
    if ($image = $this->user_picture->entity) {
      /** @var \Drupal\image\ImageStyleInterface $style */
      $style = \Drupal::entityTypeManager()
        ->getStorage('image_style')
        ->load('user_picture');
      $picture = $style->buildUrl($image->getFileUri());
    }

    return $picture;
  }

}
