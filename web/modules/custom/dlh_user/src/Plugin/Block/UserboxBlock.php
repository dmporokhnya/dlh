<?php

namespace Drupal\dlh_user\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'UserboxBlock' block.
 *
 * @Block(
 *   id = "userbox_block",
 *   admin_label = @Translation("Userbox block"),
 *   category = @Translation("Custom"),
 * )
 */
class UserboxBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->getUserEntity();

    $build = [
      '#theme' => 'dlh_user__userbox_block',
      '#username' => ucwords($user->getDisplayName()),
      '#user_picture' => $user->getPicture(),
      '#email' => $user->getEmail(),
      '#role' => ucfirst(implode(', ', $user->getRoles())),
      '#access' => !$this->currentUser->isAnonymous(),
      '#list' => $this->getLinks(),
    ];

    return $build;
  }

  /**
   * @return array
   */
  protected function getLinks() {
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->getUserEntity();

    $links[] = [
      'link' => $user->toUrl()->toString(),
      'label' => $this->t('My Profile'),
      'class' => 'user-circle',
    ];

    $links[] = [
      'link' => Url::fromRoute('user.logout')->toString(),
      'label' => $this->t('Logout'),
      'class' => 'power-off',
    ];

    return $links;
  }

  /**
   * @return \Drupal\Core\Entity\EntityInterface|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getUserEntity() {
    /** @var \Drupal\user\UserInterface $user */
    return $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
  }

}
