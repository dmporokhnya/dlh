<?php

/**
 * @file
 * Theme and preprocess functions for forms.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function rad_dlh_form_user_login_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form['name']['#attributes']['class'] = [
    'form-control',
    'form-control-lg',
  ];
  $form['name']['#attributes']['placeholder'] = $form['name']['#description'];
  $form['pass']['#attributes']['placeholder'] = $form['pass']['#description'];
  unset($form['name']['#description'], $form['pass']['#description']);
  $form['actions_wrapper'] = [
    '#type' => 'container',
    '#attributes' => [
      'class' => ['row'],
    ],
  ];
  $form['actions_wrapper']['persistent_login'] = $form['persistent_login'];
  $form['actions_wrapper']['actions'] = $form['actions'];
  unset($form['persistent_login'], $form['actions']);
  $form['actions_wrapper']['persistent_login']['#title'] = ucwords(strtolower($form['actions_wrapper']['persistent_login']['#title']));
  $form['actions_wrapper']['actions']['submit']['#value'] = t('Sign In');
  $form['social_auth'] = rad_dlh_get_social_auth_links();
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function rad_dlh_form_user_pass_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form['actions']['submit']['#value'] = t('Reset!');
  $user = \Drupal::currentUser();
  if ($user->isAnonymous()) {
    $form['name']['#attributes']['class'] = [
      'form-control',
      'form-control-lg',
    ];
    $form['name']['#attributes']['placeholder'] = $form['name']['#title'];
    unset($form['name']['#title'], $form['mail']);
    $form['name_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['input-group'],
      ],
      '#prefix' => '<div class="form-group mb-0">',
      '#suffix' => '</div>',
    ];
    $form['name_wrapper']['name'] = $form['name'];
    $form['name_wrapper']['actions'] = $form['actions'];
    unset($form['name'], $form['actions']);
    $form['remembered_link'] = rad_dlh_get_remembered_link();
    $form['remembered_link']['#prefix'] = '<p class="text-center mt-3">' . t('Remembered? ');
    $form['remembered_link']['#suffix'] = '</p>';
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function rad_dlh_form_user_register_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form['account']['name']['#weight'] = -10;
  $form['account']['name']['#title'] = t('Name');
  $form['account']['mail']['#title'] = t('E-mail Address');
  unset($form['account']['name']['#description'], $form['account']['mail']['#description'], $form['account']['pass']['#description']);
  $form['actions']['submit']['#value'] = t('Sign Up');
  $form['social_auth'] = rad_dlh_get_social_auth_links();
}

/**
 * @return array
 */
function rad_dlh_get_remembered_link() {
  return [
    '#title' => t('Sign In!'),
    '#type' => 'link',
    '#url' => Url::fromRoute('user.login'),
  ];
}

/**
 * @return array
 */
function rad_dlh_get_social_auth_links() {
  return [
    '#theme' => 'social_auth_buttons',
    '#url' => [
      'fb' => Url::fromRoute('social_auth_facebook.redirect_to_fb'),
      'google' => Url::fromRoute('social_auth_google.redirect_to_google'),
    ],
    '#weight' => 110,
  ];
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function rad_dlh_form_search_block_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form['keys']['#placeholder'] = t('Search...');
}
