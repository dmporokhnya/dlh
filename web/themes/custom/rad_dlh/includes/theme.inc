<?php

/**
 * @file
 * Custom theme hooks.
 */

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function rad_dlh_theme_suggestions_container_alter(array &$suggestions, array $variables) {
  $element = $variables['element'];

  if (isset($element['#type']) && $element['#type'] == 'actions') {
    $suggestions[] = 'container__actions';
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function rad_dlh_preprocess_page(&$variables) {
  $variables['path_logo'] = '/' . \Drupal::theme()->getActiveTheme()->getPath() . '/assets/img/logo.png';
}

/**
 * Implements hook_theme().
 */
function rad_dlh_theme($existing, $type, $theme, $path) {
  return [
    'social_auth_buttons' => [
      'variables' => [
        'url' => [],
      ],
    ],
  ];
}
